
var landTransfer = angular.module('landTransfer',['ngCookies']);
landTransfer.run([ '$http',  function ($http) {
    $http.defaults.headers.post["Accept"] = "";
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

}]);
landTransfer.controller('idManagementPage', function ($scope, $http, $cookieStore, $timeout, $window) {
    //id management page form scope
    $scope.residentDetails = {
        fullName:'',
        age: '',
        emiratesId:'',
        email:'',
        contactNumber:'',
        occupation:''
    };
    // show hide scope
    $scope.ngShow = {
        idManageLogin:true,
        idMangeForm:false,
        listOptions:false,
        updateSuccess:false
    };
    //login credentials scope
    $scope.login = {
        email:'',
        password:''
    };
    //modal scope
    $scope.modal = {
        residentList:'',
        userAddress:'',
        userData:''
    };
    /* Menu redirections [start]*/
    $scope.propertyManage = function () {
        window.location.href = 'propertyMagement.html';
    };
    $scope.UserLogin = function () {
        window.location.href = 'user.html';
    };
    $scope.PoC = function () {
        window.location.href = 'home.html';
    };
    //check login [start]
    //get resident list function [start]
    $scope.getResident = function () {
        var requestObj = {
            method: 'GET',
            url: server + 'api/v1/getUsers',
        };
        $http(requestObj).success(function (data) {
            $scope.modal.residentList = data.result;
            console.log($scope.residentList)
            console.log(data)
        }).error(function (data, err) {
            console.log(data, err);
        });
    };
    //get resident list function [end]
    $scope.loginChaeck = function () {
        var loginStatus = $cookieStore.get('resident');
        if (loginStatus == 'true') {
            $scope.ngShow.idManageLogin = false;
            $scope.ngShow.idMangeForm = true;
            $scope.getResident();
        } else if(loginStatus == 'false') {
            $scope.ngShow.idManageLogin = true;
            $scope.ngShow.idMangeForm = false;
        }
    };
    $scope.loginChaeck();
    //check login [end]
    //idManageLogin click function [start]
    $scope.idManageLogin = function () {
        if (!$scope.login.email) {
            showError('Please enter email', 'error', true);
            return false;
        }
        var EMAIL_REGEXP = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}/;
        if ($scope.login.email) {
            var checktype = $scope.login.email.charAt(0);
            checktype = isNaN(checktype);
            if (!EMAIL_REGEXP.test($scope.login.email)) {
                showError('Email not valid', 'error', true);
                return false;
            }
            if (!checktype) {
                showError('Email not valid', 'error', true);
                return false;
            }
        }
        if (!$scope.login.password) {
            showError('Please enter password', 'error', true);
            return false;
        }
        var data = 'password=' + $scope.login.password + '&&email=' + $scope.login.email + '&&type=1';
        // var data = 'password=123456'+ '&&email=rahul@accubits.com' + '&&type=1';
        var postData = data;
        var requestObj = {
            method: 'POST',
            url: server + 'api/v1/signIn',
            data: postData
        };
        $http(requestObj).success(function (data) {
            if (data.status_cd == 400) {
                showError('Please enter valid email and  password', 'error', true);
                return false;
            }
            if (!data.success) {
                showError('Please enter valid email and  password', 'error', true);
                return false;
            }
            $scope.ngShow.idManageLogin = false;
            $scope.ngShow.idMangeForm = true;
            $scope.getResident();
            $cookieStore.put('resident', 'true');
            console.log(data)
        }).error(function (data, err) {
            console.log(data, err);
            showError('Email not valid', 'error', true);
            return false;
        });
    };
    //idManageLogin click function [end]
    //show resident list [start]
    $scope.showList = function () {
        $timeout( function(){
            $scope.ngShow.listOptions = true
        }, 50);
    };
    $scope.hideList = function () {
          if ($scope.ngShow.listOptions == true) {
              $scope.ngShow.listOptions = false;
          }
    };
    //show resident list [end]
    //select resident list function [start]
    $scope.selectResident = function (address) {
        $scope.modal.userAddress = address;
        var data = 'userAddress=' + $scope.modal.userAddress;
        var postData = data;
        var requestObj = {
            method: 'POST',
            url: server + 'api/v1/getUserDetails',
            data: postData
        };
        $http(requestObj).success(function (data) {
            $scope.modal.userData = data.data.result;
            $scope.ngShow.listOptions = false;
            $scope.residentDetails.fullName = $scope.modal.userData[0];
            $scope.residentDetails.age = parseInt($scope.modal.userData[1]);
            $scope.residentDetails.occupation = $scope.modal.userData[2];
            $scope.residentDetails.emiratesId = $scope.modal.userData[3];
            $scope.residentDetails.email = $scope.modal.userData[4];
            $scope.residentDetails.contactNumber = $scope.modal.userData[5];
            console.log(data)

        }).error(function (data, err) {
            console.log(data)
            console.log(err)
        });
    };
    //select resident list function [end]
    //update resident list function [start]
    $scope.idManagementSubmit = function () {
        var data = 'userAddress=' + $scope.modal.userAddress + '&&name='+ $scope.residentDetails.fullName + '&&age=' + $scope.residentDetails.age + '&&occupation=' + $scope.residentDetails.occupation + '&&password=Test12345' +
                    '&&contactNumber=' + $scope.residentDetails.contactNumber + '&&email=' + $scope.residentDetails.email + '&&emiratesId=' +$scope.residentDetails.emiratesId ;
        var postData = data;
        var requestObj = {
            method: 'POST',
            url: server + 'api/v1/updateResident',
            data: postData
        };
        $http(requestObj).success(function (data) {
            console.log(data)
            $scope.ngShow.updateSuccess = true
        }).error(function (data, err) {
            console.log(data)
            console.log(err)
        });
    };
    //update resident list function [end]
    //close update resident success [start]
    $scope.updateSuccessOk = function () {
        $scope.ngShow.updateSuccess = false
    }
    //close update resident success [end]
    //logout [end]
    $scope.Logout = function () {
        $scope.ngShow.idManageLogin = true;
        $scope.ngShow.idMangeForm = false;
        $cookieStore.put('resident','false')
    }
    //logout [end]
});