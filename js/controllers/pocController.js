
var landTransfer = angular.module('landTransfer',['ngCookies']);
landTransfer.run([ '$http',  function ($http) {
    $http.defaults.headers.post["Accept"] = "";
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

}]);
landTransfer.controller('poc', function ($scope, $http, $cookieStore, $timeout, $window) {
    $scope.contactEmail = '';
    /* Menu redirections [start]*/
    $scope.idManage = function () {
        window.location.href = 'idManagement.html';
    };
    $scope.propertyManage = function () {
        window.location.href = 'propertyMagement.html';
    };
    $scope.UserLogin = function () {
        window.location.href = 'user.html';
    };
    $scope.PoC = function () {
        window.location.href = 'home.html';
    };
    /* Menu redirections[end]*/
    $scope.indexGo = function () {
        if (!$scope.contactEmail) {
            showError('Please enter email', 'error', true);
            return false;
        }

        var EMAIL_REGEXP = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}/;

        if ($scope.contactEmail) {
            var checktype = $scope.contactEmail.charAt(0);
            checktype = isNaN(checktype);

            if (!EMAIL_REGEXP.test($scope.contactEmail)) {
                showError('Email not valid', 'error', true);
                return false;
            }
            if (!checktype) {
                showError('Email not valid', 'error', true);
                return false;
            }
        }
        $window.location.href = '../../idManagement.html';
    }
});