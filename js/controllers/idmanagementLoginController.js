
var landTransfer = angular.module('landTransfer',['ngCookies']);
landTransfer.run([ '$http',  function ($http) {
    $http.defaults.headers.post["Accept"] = "";
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

}]);
landTransfer.controller('generalUserLogin', function ($scope, $http, $cookieStore, $timeout, $window) {
    // show hide scope
    $scope.ngShow = {
        idManageLogin:true,
        idMangeForm:false,
        listOptions:false,
        updateSuccess:false,
        buyList:true,
        buyOrders:false
    };
    //login credentials scope
    $scope.login = {
        email:'',
        password:''
    };
    //modal scope
    $scope.modal = {
        residentList:'',
        userAddress:'',
        userData:'',
        Properties:[],
        buyerDetails:[]
    };
    /* Menu redirections [start]*/
    $scope.idManage = function () {
        window.location.href = 'idManagement.html';
    };
    $scope.propertyManage = function () {
        window.location.href = 'propertyMagement.html';
    };
    $scope.PoC = function () {
        window.location.href = 'home.html';
    };
    //check login [start]
    //get resident list function [start]
    $scope.getResident = function () {
        var data = 'address=' + $scope.modal.userAddress;
        var postData = data;
        var requestObj = {
            method: 'POST',
            url: server + 'api/v1/getUserPropertyList',
            data: postData
        };
        $http(requestObj).success(function (data) {
            console.log(data)
            $scope.modal.residentList = data.result;
            console.log( $scope.modal.residentList);
            var PropertyId = $scope.modal.residentList.propertyId;
            console.log(data)
            var i;
            for(i=0; i <= $scope.modal.residentList.length-1; i++){
                // console.log(i);
                // console.log($scope.modal.residentList[i]["propertyId"])
                // console.log($scope.modal.residentList[i]["address"])
                $scope.getPropertyLists($scope.modal.residentList[i]["propertyId"], $scope.modal.residentList[i]["address"])
            }

        }).error(function (data, err) {
            console.log(data, err);
        });
    };
    //get resident list function [end]
    $scope.loginChaeck = function () {
        var loginStatus = $cookieStore.get('user');
        var userAdderss = $cookieStore.get('userAddress');
        if (loginStatus == 'true') {
            $scope.ngShow.idManageLogin = false;
            $scope.ngShow.idMangeForm = true;
            $scope.modal.userAddress = userAdderss;
            $scope.getResident()
        } else if(loginStatus == 'false') {
            $scope.ngShow.idManageLogin = true;
            $scope.ngShow.idMangeForm = false;
        }
    };
    $scope.loginChaeck();
    //idManageLogin click function [start]
    $scope.idManageLogin = function () {
        if (!$scope.login.email) {
            showError('Please enter email', 'error', true);
            return false;
        }
        var EMAIL_REGEXP = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}/;
        if ($scope.login.email) {
            var checktype = $scope.login.email.charAt(0);
            checktype = isNaN(checktype);
            if (!EMAIL_REGEXP.test($scope.login.email)) {
                showError('Email not valid', 'error', true);
                return false;
            }
            if (!checktype) {
                showError('Email not valid', 'error', true);
                return false;
            }
        }
        if (!$scope.login.password) {
            showError('Please enter password', 'error', true);
            return false;
        }
        var data = 'password=' + $scope.login.password + '&&email=' + $scope.login.email + '&&type=3';
        // var data = 'password=123456'+ '&&email=rahul123@accubits.com' + '&&type=3';
        var postData = data;
        var requestObj = {
            method: 'POST',
            url: server + 'api/v1/signIn',
            data: postData
        };
        $http(requestObj).success(function (data) {
            if (data.status_cd == 400) {
                showError('Please enter valid email and  password', 'error', true);
                return false;
            }
            if (!data.success) {
                showError('Please enter valid email and  password', 'error', true);
                return false;
            }
            console.log(data)
            $scope.modal.userAddress = data.result.address;
            $scope.ngShow.idManageLogin = false;
            $scope.ngShow.idMangeForm = true;
            $scope.getResident();
            $cookieStore.put('user', 'true');
            $cookieStore.put('userAddress', data.result.address);
            console.log(data)
        }).error(function (data, err) {
            console.log(data, err);
            showError('Email not valid', 'error', true);
            return false;
        });
    };
    //idManageLogin click function [end]
    //get list for properties function [start]
    $scope.getPropertyLists = function (id, address) {
        var data = 'propertyId=' + id;
        var postData = data;
        var requestObj = {
            method: 'POST',
            url: server + 'api/v1/getUserPropertyDetails',
            data: postData
        };
        $http(requestObj).success(function (data) {
            console.log(data)
            data.data.result["id"] = id;
            data.data.result["address"] = address;
            $scope.modal.Properties.push(data.data.result)
            console.log($scope.modal.Properties)
        }).error(function (data, err) {
            console.log(data, err);
        });
    };
    //get list for properties function [end]
    //buy click function [start]
    $scope.buyProperty = function (id, addess) {
        var data = 'currentUserAddress=' + addess + '&&propertyId=' + id + '&&newOwnerAddress=' + $scope.modal.userAddress + '&&password=Test12345';
        var postData = data;
        var requestObj = {
            method: 'POST',
            url: server + 'api/v1/buyRequest',
            data: postData
        };
        $http(requestObj).success(function (data) {
            console.log($scope.modal.userAddress)
            console.log(data)
            if (data.data.status_cd == 400) {
                showError('Error, Try again later', 'error', true);
                return false;
            }
            $scope.ngShow.updateSuccess = true;
        }).error(function (data, err) {
            console.log(data, err);
        });
    };
    //buy click function [end]
    //close update resident success [start]
    $scope.buySuccessOk = function () {
        $scope.ngShow.updateSuccess = false
    }
    //close update resident success [end]
    //propertiesListbtn nav click [start]
    $scope.propertiesListbtn = function () {
        $scope.ngShow.buyList = true;
        $scope.ngShow.buyOrders = false;
        $('.propertyNav_one').addClass("propertyNav_btn_selected")
        $('.propertyNav_two').removeClass("propertyNav_btn_selected")
    };
    $scope.propertiesListbtnbuy = function () {
        var data = 'userAddress=' + $scope.modal.userAddress;
        var postData = data;
        var requestObj = {
            method: 'POST',
            url: server + 'api/v1/buyRequestCheck',
            data: postData
        };
        $http(requestObj).success(function (data) {
            if (data.data.status_cd == 400) {
                showError('Error, Try again later', 'error', true);
                return false;
            }
            $scope.getBuyerdetails(data.data.result[0], data.data.result[1])
            console.log(data)
            console.log(data.data.result[0], data.data.result[1])
        }).error(function (data, err) {
            console.log(data, err);
        });
        $scope.ngShow.buyList = false;
        $scope.ngShow.buyOrders = true;
        $('.propertyNav_one').removeClass("propertyNav_btn_selected");
        $('.propertyNav_two').addClass("propertyNav_btn_selected");
    }
    //cpropertiesListbtn nav click [end]
    //get buyer details function [start]
    $scope.getBuyerdetails = function (address, id) {
        var data = 'propertyId=' + id;
        var postData = data;
        var requestObj = {
            method: 'POST',
            url: server + 'api/v1/getUserPropertyDetails',
            data: postData
        };
        $http(requestObj).success(function (data) {
            data.data.result["id"] = id;
            data.data.result["address"] = address;
           $scope.modal.buyerDetails.push(data.data.result)
            console.log($scope.modal.buyerDetails)
        }).error(function (data, err) {
            console.log(data, err);
        });
    }
    //get buyer details function [end]
    //buy confirm button click [start]
    $scope.buyConfirm = function () {
        var data = 'userAddress=' + $scope.modal.userAddress + '&&password=Test12345';
        var postData = data;
        var requestObj = {
            method: 'POST',
            url: server + 'api/v1/buyRequestUpdate',
            data: postData
        };
        $http(requestObj).success(function (data) {
            $scope.ngShow.updateSuccess = true;
            console.log(data)
        }).error(function (data, err) {
            console.log(data, err);
        });
    }
    //buy confirm button click [end]
    //logout [end]
    $scope.Logout = function () {
        $scope.ngShow.idManageLogin = true;
        $scope.ngShow.idMangeForm = false;
        $cookieStore.put('user', 'false');
    }
    //logout [end]
});