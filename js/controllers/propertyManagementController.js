
var landTransfer = angular.module('landTransfer',['ngCookies']);
landTransfer.run([ '$http',  function ($http) {
    $http.defaults.headers.post["Accept"] = "";
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

}]);
landTransfer.controller('propertyManagement', function ($scope, $http, $cookieStore, $timeout, $window) {
    //id management page form scope
    $scope.residentDetails = {
        propertyId:'',
        sqfeet: '',
        totalArea:'',
        gpsCoordintes:'',
        landmark:'',
        propertyOwner:''
    };
    // show hide scope
    $scope.ngShow = {
        idManageLogin:true,
        idMangeForm:false,
        listOptions:false,
        updateSuccess:false,
        updateSuccess2:false,
        PropertyListing:false
    };
    //login credentials scope
    $scope.login = {
        email:'',
        password:''
    };
    //modal scope
    $scope.modal = {
        residentList:'',
        userAddress:'',
        propertyId:'',
        userData:'',
        listPropertyDetails:[]
    };
    /* Menu redirections [start]*/
    $scope.idManage = function () {
        window.location.href = 'idManagement.html';
    };
    $scope.UserLogin = function () {
        window.location.href = 'user.html';
    };
    $scope.PoC = function () {
        window.location.href = 'home.html';
    };
    //get resident list function [start]
    $scope.getProperty = function () {
        var requestObj = {
            method: 'GET',
            url: server + 'api/v1/getPropertyList'
        };
        $http(requestObj).success(function (data) {
            $scope.modal.residentList = data.result;
            console.log($scope.modal.residentList)
            console.log(data)
        }).error(function (data, err) {
            console.log(data, err);
        });
    };
    //get resident list function [end]
    //check login [start]
    $scope.loginChaeck = function () {
        var loginStatus = $cookieStore.get('property');
        if (loginStatus == 'true') {
            $scope.ngShow.idManageLogin = false;
            $scope.ngShow.idMangeForm = true;
            $scope.getProperty();
        } else if(loginStatus == 'false') {
            $scope.ngShow.idManageLogin = true;
            $scope.ngShow.idMangeForm = false;
        }
    };
    $scope.loginChaeck();
    //check login [end]
    //PropertyManageLogin click function [start]
    $scope.PropertyManageLogin = function () {
        if (!$scope.login.email) {
            showError('Please enter email', 'error', true);
            return false;
        }
        var EMAIL_REGEXP = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}/;
        if ($scope.login.email) {
            var checktype = $scope.login.email.charAt(0);
            checktype = isNaN(checktype);
            if (!EMAIL_REGEXP.test($scope.login.email)) {
                showError('Email not valid', 'error', true);
                return false;
            }
            if (!checktype) {
                showError('Email not valid', 'error', true);
                return false;
            }
        }
        if (!$scope.login.password) {
            showError('Please enter password', 'error', true);
            return false;
        }
        var data = 'password=' + $scope.login.password + '&&email=' + $scope.login.email + '&&type=2';
        // var data = 'password=123456'+ '&&email=pm@accubits.com' + '&&type=2';
        var postData = data;
        var requestObj = {
            method: 'POST',
            url: server + 'api/v1/signIn',
            data: postData
        };
        $http(requestObj).success(function (data) {
            if (data.status_cd == 400) {
                showError('Please enter valid email and  password', 'error', true);
                return false;
            }
            if (!data.success) {
                showError('Please enter valid email and  password', 'error', true);
                return false;
            }
            $scope.ngShow.idManageLogin = false;
            $scope.ngShow.idMangeForm = true;
            $scope.getProperty();
            $cookieStore.put('property', 'true');
            console.log(data)
        }).error(function (data, err) {
            console.log(data, err);
        });
    };
    //PropertyManageLogin click function [end]
    //show resident list [start]
    $scope.showList = function () {
        $timeout( function(){
            $scope.ngShow.listOptions = true
        }, 50);
    };
    $scope.hideList = function () {
        if ($scope.ngShow.listOptions == true) {
            $scope.ngShow.listOptions = false;
        }
    };
    //show resident list [end]
    //select resident list function [start]
    $scope.selectProperty = function (id, address) {
        $scope.modal.propertyId = id;
        $scope.modal.userAddress = address;
        console.log(address);
        var data = 'propertyId=' + $scope.modal.propertyId;
        var postData = data;
        var requestObj = {
            method: 'POST',
            url: server + 'api/v1/getUserPropertyDetails',
            data: postData
        };
        $http(requestObj).success(function (data) {
            $scope.modal.userData = data.data.result;
            $scope.ngShow.listOptions = false;
            $scope.residentDetails.sqfeet = $scope.modal.userData[0];
            $scope.residentDetails.totalArea = $scope.modal.userData[1];
            $scope.residentDetails.gpsCoordintes = $scope.modal.userData[2];
            $scope.residentDetails.landmark = $scope.modal.userData[3];
            $scope.residentDetails.propertyId = $scope.modal.propertyId;
            $scope.residentDetails.propertyOwner = $scope.modal.userData[4];
            console.log($scope.modal.userData)
        }).error(function (data, err) {
            console.log(data)
        });
    };
    //select resident list function [end]
    //update resident list function [start]
    $scope.propertyManagementSubmit = function () {
        var data = 'userAddress=' + $scope.modal.userAddress + '&&propertyId='+ $scope.residentDetails.propertyId + '&&sqfeet=' + $scope.residentDetails.sqfeet + '&&totalArea=' + $scope.residentDetails.totalArea + '&&password=Test12345' +
            '&&gpsCoordinates=' + $scope.residentDetails.gpsCoordintes + '&&landmark=' + $scope.residentDetails.landmark ;
        var postData = data;
        var requestObj = {
            method: 'POST',
            url: server + 'api/v1/updateProperty',
            data: postData
        };
        $http(requestObj).success(function (data) {
            console.log(data)
            $scope.ngShow.updateSuccess = true
        }).error(function (data, err) {
            console.log(data)
            console.log(err)
        });
    };
    //update resident list function [end]
    //close update resident success [start]
    $scope.updateSuccessOk = function () {
        $scope.ngShow.updateSuccess = false
    };
    //close update resident success [end]
    //propertyBackbtn [start]
    $scope.propertyBackbtn = function () {
        $scope.ngShow.PropertyListing = false;
        $scope.ngShow.idMangeForm = true;
    }
    //propertyBackbtn [end]
    // propertyForwardBtn [start]
    $scope.propertyForwardBtn = function () {
        var requestObj = {
            method: 'GET',
            url: server + 'api/v1/getRequestCount',
        };
        $http(requestObj).success(function (data) {
            var i;
            for (i=0; i<data.data["count"]; i++){
                console.log(i)
                $scope.getUserAdderss(i)
            }
            // console.log(data)
            // console.log(data.data["count"])
        }).error(function (data, err) {
            console.log(data)
            console.log(err)
        });
        $scope.ngShow.PropertyListing = true;
        $scope.ngShow.idMangeForm = false;
    }
    //propertyForwardBtn [end]
    //get user's addresses of transfer request [START]
        $scope.getUserAdderss = function (id) {
            var data = 'id=' + id;
            var postData = data;
            var requestObj = {
                method: 'POST',
                url: server + 'api/v1/getRequestAddressAtIndex',
                data: postData
            };
            $http(requestObj).success(function (data) {
                // console.log(data)
                $scope.viewTransferreq(data.data["userAddress"])
            }).error(function (data, err) {
                console.log(data)
            });
        };
    //get user's addresses of transfer request [end]
    //view transfer request by admin [end]
    $scope.viewTransferreq = function (address) {
        var data = 'userAddress=' + address;
        var postData = data;
        var requestObj = {
            method: 'POST',
            url: server + 'api/v1/buyRequestConfirm',
            data: postData
        };
        $http(requestObj).success(function (data) {
            console.log(data)
            console.log(data.data.result[1])

            $scope.getUserPropList(data.data.result[1],data.data.result[0])
        }).error(function (data, err) {
            console.log(data)
        });
    };
    //view transfer request by admin [end]
    //get user property details [start]
    $scope.getUserPropList = function (id, address) {
        var data = 'propertyId=' + id;
        var postData = data;
        var requestObj = {
            method: 'POST',
            url: server + 'api/v1/getUserPropertyDetails',
            data: postData
        };
        $http(requestObj).success(function (data) {
            console.log(data)
            data.data.result["id"] = id;
            data.data.result["address"] = address;
            $scope.modal.listPropertyDetails.push(data.data.result)
            console.log( $scope.modal.listPropertyDetails)
        }).error(function (data, err) {
            console.log(data)
        });
    };
    //get user property details [end]
    //exicuteTransfer [start]
    $scope.exicuteTransfer = function (id, address) {
        var data = 'propertyId=' + id + '&&newOwnerAddress=' +address + '&&password=Test12345';
        var postData = data;
        var requestObj = {
            method: 'POST',
            url: server + 'api/v1/update',
            data: postData
        };
        $http(requestObj).success(function (data) {
            console.log(data)
            if (data.status_cd == 400) {
                showError('Error, please try again later', 'error', true);
                return false;
            }
            $scope.ngShow.updateSuccess2 = true
            console.log(data)
        }).error(function (data, err) {
            console.log(data)
        });
    }
    //exicuteTransfer [end]
    //close update resident success [start]
    $scope.buySuccessOk = function () {
        $scope.ngShow.updateSuccess = false
        $scope.ngShow.updateSuccess2 = false
    }
    //close update resident success [end]
    //logout [end]
    $scope.Logout = function () {
        $scope.ngShow.idManageLogin = true;
        $scope.ngShow.idMangeForm = false;
        $cookieStore.put('property', 'false');
    }
    //logout [end]
});