/**
 * Created by arun on 5/10/16.
 * arunjayakumar07@gmail.com
 */



function addCSSRule(sheet, selector, rules, index) {
	try{
		 if ("insertRule" in sheet) {
        sheet.insertRule(selector + "{" + rules + "}", index);
    }
    else if ("addRule" in sheet) {
        sheet.addRule(selector, rules, index);
    }
	}
	catch(e){
		console.log(e);
	}
   
}

function initToast() {
	
	try{

        var html = '<div class="toast_outer">'+
            '<div class="toast">'+
            // '<div class="toast_icon">'+
            // '<img class="error_img" src="images/checked%20green.png">'+
            // '<img class="load_img" src="images/checked%20green.png">'+
            // '<img class="success_img" src="images/checked%20green.png">'+
            // '</div>'+
            '<div class="msg">Saved successfully!</div>'+
            '</div>'+
            '</div>';

    $('body').append(html);

    var style = document.createElement("style");
    style.appendChild(document.createTextNode(""));
    document.head.appendChild(style);
    var sheet = document.styleSheets[0];

        addCSSRule(sheet,
        '.toast_outer',
        'position: fixed;'+
        'bottom:-100px;'+
        'opacity:0;'+
        'left:0px;'+
        'z-index: -100;'+
        'width: 100%;'+
        'text-align: center;'+
        '-webkit-transition-duration: 700ms;'+
        'transition-duration: 700ms;'
            , 0);
        addCSSRule(sheet,
            '.toast_outer.show',
            'bottom:30px;'+
            'z-index: 100;'+
            'opacity:1;'
            , 1);

        addCSSRule(sheet,
            '.toast',
            'position: relative;'+
            'margin: auto;'+
            'background-color: rgba(37, 46, 54, 0.9);'+
            'font-size: 12px;'+
            'color: white;'+
            'white-space: normal;'+
            'line-height: 20px;'+
            'padding: 11px 29px 11px 50px;'+
            'border-radius: 7px;'+
            'max-width: 50%;'+
            'min-width: 100px;'+
            'display: inline-block;'+
            'text-align: left;'+
            'padding-left: 50px;'
            , 2);

        addCSSRule(sheet,
            '.toast_icon',
            ' display: inline-block;'+
            'vertical-align: middle;'+
            'margin-right: 18px;'+
            'width: 22px;'+
            'top: 12px;'+
            'position: absolute;'+
            'left: 15px;'
            , 3);
        addCSSRule(sheet,
            '.toast_icon img',
            'width: 20px;'+
            'display: none;'
            , 4);
        addCSSRule(sheet,
            '.error .toast_icon img.error_img',
            'display: block;'
            , 5);
        addCSSRule(sheet,
            '.loader .toast_icon img.load_img',
            'display: block;'
            , 6);
        addCSSRule(sheet,
            '.success .toast_icon img.success_img',
            'display: block;'
            , 7);

    //sheet.insertRule(
    //    '@-webkit-keyframes error {' +
    //    '0%   {' +
    //    '-webkit-transform: scale(1);-moz-transform: scale(1);-ms-transform: scale(1);-o-transform: scale(1);transform: scale(1);' +
    //    '}' +
    //    '50%{' +
    //    '-webkit-transform: scale(1.1);-moz-transform: scale(1.1);-ms-transform: scale(1.1);-o-transform: scale(1.1);transform: scale(1.1);' +
    //    '}' +
    //    '100% {' +
    //    '-webkit-transform: scale(1);-moz-transform: scale(1);-ms-transform: scale(1);-o-transform: scale(1);transform: scale(1);' +
    //    '}' +
    //    '}',
    //    1
    //);
    //
    //
    //sheet.insertRule(
    //    "@keyframes error{ " +
    //    "0%   {" +
    //    "-webkit-transform: scale(1);-moz-transform: scale(1);-ms-transform: scale(1);-o-transform: scale(1);transform: scale(1);" +
    //    "}" +
    //    "50%{ " +
    //    "-webkit-transform: scale(1.1);-moz-transform: scale(1.1);-ms-transform: scale(1.1);-o-transform: scale(1.1);transform: scale(1.1);" +
    //    "} " +
    //    "100% { " +
    //    "-webkit-transform: scale(1);-moz-transform: scale(1);-ms-transform: scale(1);-o-transform: scale(1);transform: scale(1);" +
    //    "} " +
    //    "} ",
    //    2
    //);

	}
	catch(e){
		console.log(e);
	}

}


function showError(msg, type, hide) {
	
	try{
	

    $('.toast').attr('class','toast');
    $('.toast .msg').html(msg);

    switch (type) {
        case 'success':
            $('.toast').addClass('success');
            break;
        case 'error':
            $('.toast').addClass('error');
            break;
        case 'loading':
            $('.toast').addClass('loader');
            break;
    }

    $('.toast_outer').addClass('show');

    if (hide) {
        setTimeout(function () {
            $('.toast_outer').removeClass('show');
        }, 2000);
    }
		
	}
	catch(e){
		console.log(e);
	}
}
function hideError() {
	try{
        $('.toast_outer').removeClass('show');	}
	catch(e){
		console.log(e);
	}
}


window.onload = initToast();
